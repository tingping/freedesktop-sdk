image: buildstream/buildstream-fedora:master-81-06ae434

variables:
  # Store everything under the /builds directory. This is a separate Docker
  # volume. Note that GitLab CI will only cache stuff inside the build
  # directory.
  XDG_CACHE_HOME: "${CI_PROJECT_DIR}/cache"
  GET_SOURCES_ATTEMPTS: 3
  BST_CACHE_SERVER_ADDRESS: 'cache.sdk.freedesktop.org'
  BST_CACHE_SSH_USER: 'artifacts'

  # Generic variable for invoking buildstream
  BST: bst --colors

stages:
  - build
  - flatpak
  - vm
  - publish_x86_64
  - publish_i586
  - publish_aarch64
  - publish_arm

before_script:
  - export PATH=~/.local/bin:${PATH}
  - export BST_SHA='0916d81e25bb57e4f6d8109fefcbd0ed8f33c371' #  1.1.3-4-g0916d81e
  - git clone https://gitlab.com/BuildStream/buildstream.git
  - git -C buildstream checkout $BST_SHA
  - pip3 install --user buildstream/

  # Create ~/.ssh for storing keys
  - mkdir -p ~/.ssh

  # Private key stored as a protected variable that allows pushing to
  # artifacts@cache.sdk.freedesktop.org
  - |
    if [ -z "$freedesktop_ostree_cache_private_key" ]; then
        echo >&2 "Private key for cache.sdk.freedesktop.org is not available."
    else
        echo "$freedesktop_ostree_cache_private_key" > ~/.ssh/id_rsa
        chmod 600 ~/.ssh/id_rsa
        ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub
    fi

  # Use internal network only if we are using x86_64 runners from DigitalOcean
  - |
    if [ "${CI_RUNNER_DESCRIPTION}" == 'manager-runner-freedesktop' ]; then
        export BST_CACHE_SERVER_ADDRESS="${GITLAB_BST_CACHE_IP}"
        export BST_CACHE_SSH_USER='artifacts-internal'
    fi

  # Trust the host key of the cache server.
  - ssh-keyscan "${BST_CACHE_SERVER_ADDRESS}" >> ~/.ssh/known_hosts

  # If we can push, then enable push and pull for freedesktop-sdk artifact cache
  # (default config is pull only)
  - |
    if [ -n "$freedesktop_ostree_cache_private_key" ]; then
        mkdir -p ~/.config
        echo "projects:" > ~/.config/buildstream.conf
        echo "  base-sdk-bootstrap:" >> ~/.config/buildstream.conf
        echo "    artifacts:" >> ~/.config/buildstream.conf
        echo "      url: ssh://${BST_CACHE_SSH_USER}@${BST_CACHE_SERVER_ADDRESS}/artifacts/" >> ~/.config/buildstream.conf
        echo "      push: true" >> ~/.config/buildstream.conf
        echo "  base-sdk:" >> ~/.config/buildstream.conf
        echo "    artifacts:" >> ~/.config/buildstream.conf
        echo "      url: ssh://${BST_CACHE_SSH_USER}@${BST_CACHE_SERVER_ADDRESS}/artifacts/" >> ~/.config/buildstream.conf
        echo "      push: true" >> ~/.config/buildstream.conf
    fi

  # Go easy on the artifact server. The fixed unchangeable connection
  # timeout for ostree is 30 seconds. And it cannot be changed.
  - |
    [ -d "${HOME}/.config"] || mkdir -p "${HOME}/.config"
    cat >>"${HOME}/.config/buildstream.conf" <<EOF
    scheduler:
      fetchers: 5
      network-retries: 4
    EOF

# Store all the downloaded git and ostree repos in the distributed cache.
# This saves us fetching them on every build
.gitlab_cache_template_pull: &gitlab_cache_pull
  cache:
    key: bst
    paths:
      - "${XDG_CACHE_HOME}/buildstream/sources/"
    policy: pull

.gitlab_cache_template_pull_push: &gitlab_cache_pull_push
  cache:
    key: bst
    paths:
      - "${XDG_CACHE_HOME}/buildstream/sources/"



.build_template: &build_definition
  stage: build
  script:
    - cd "${CI_PROJECT_DIR}"/sdk
    - ${BST} -o target_arch "${ARCH}" build all.bst desktop-platform-image.bst
    - ${BST}  -o target_arch "${ARCH}" checkout desktop-platform-image.bst ./desktop-platform-image
    - bash ../utils/scan-for-dev-files.sh ./desktop-platform-image | sort -u >found_so_files.txt
    - |
      if [ -s found_so_files.txt ]; then
        echo "Found development .so files:" 1>&2
        cat found_so_files.txt 1>&2
        false
      fi
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
    - master
  <<: *gitlab_cache_pull

build_x86_64:
  <<: *build_definition
  tags:
    - x86_64
  variables:
    ARCH: x86_64

build_i586:
  <<: *build_definition
  tags:
    - x86_64
  variables:
    ARCH: i586

build_aarch64:
  image: buildstream/buildstream-fedora:aarch64-master-81-06ae434
  <<: *build_definition
  tags:
    - aarch64
  variables:
    ARCH: aarch64

build_arm:
  image: buildstream/buildstream-fedora:aarch64-master-81-06ae434
  <<: *build_definition
  tags:
    - armhf
  variables:
    ARCH: arm


.flatpak_template: &flatpak_definition
  stage: flatpak
  script:
    - cd "${CI_PROJECT_DIR}"/sdk
    - ${BST} -o target_arch "${ARCH}" build all.bst

    - echo "Export runtimes to a ostree repo"
    - mkdir runtimes
    - |
      for runtime in sdk platform; do
        bst -o target_arch "${ARCH}" checkout "${runtime}.bst" "runtimes/${runtime}";
      done
    - cd ${CI_PROJECT_DIR}

    - echo "Use flatpak builder to export the runtimes to a ostree repo"
    - dnf install -y flatpak flatpak-builder
    - export FLATPAK_USER_DIR="${PWD}/tmp-flatpak"
    - flatpakarch="${ARCH/i586/i386}"
    - flatpak build-export --arch=${ARCH} --files=files repo/ sdk/runtimes/sdk unstable;
    - flatpak build-export --arch=${ARCH} --files=files repo/ sdk/runtimes/platform unstable;

    - echo "Locally install generated flatpak runtimes"
    - flatpak remote-add --if-not-exists --user --no-gpg-verify test-repo repo/
    - flatpak install --arch="${flatpakarch}" --user test-repo runtime/org.freedesktop.Sdk//unstable
    - flatpak install --arch="${flatpakarch}" --user test-repo runtime/org.freedesktop.Platform//unstable

    - echo "Build basic flatpak app"
    - flatpak-builder --arch="${flatpakarch}" build_folder tests/org.flatpak.Hello.json

    - echo "Run basic application"
    - flatpak-builder --arch="${flatpakarch}" --run build_folder tests/org.flatpak.Hello.json hello.sh
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
    - master
  <<: *gitlab_cache_pull

app_x86_64:
  dependencies:
    - build_x86_64
  <<: *flatpak_definition
  tags:
    - x86_64
  variables:
    ARCH: x86_64

app_aarch64:
  image: buildstream/buildstream-fedora:aarch64-master-81-06ae434
  dependencies:
    - build_aarch64
  <<: *flatpak_definition
  tags:
    - aarch64
  variables:
    ARCH: aarch64

app_arm:
  image: buildstream/buildstream-fedora:aarch64-master-81-06ae434
  dependencies:
    - build_arm
  <<: *flatpak_definition
  tags:
    - armhf
  variables:
    ARCH: arm


.vm_image_template: &vm_image
  stage: vm
  script:
    - export BST_EXTERNAL_SHA=db543278fae6cc12b2ef4cfb0f68b5326ac9dd76
    - git clone https://gitlab.com/BuildStream/bst-external.git
    - git -C bst-external checkout $BST_EXTERNAL_SHA
    - pip3 install --user ./bst-external
    - cd sdk
    - ${BST} -o target_arch "${ARCH}" build vm/"${TYPE}"-vm-image-"${ARCH}".bst
    - ${BST} -o target_arch "${ARCH}" checkout vm/"${TYPE}"-vm-image-"${ARCH}".bst ./vm
    - dnf install -y qemu-system-x86
    - ../utils/test-minimal-system vm/sda.img
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
    - master
  <<: *gitlab_cache_pull

minimal_vm_image_x86_64:
  tags:
    - x86_64
  <<: *vm_image
  variables:
    ARCH: x86_64
    TYPE: minimal

minimal_systemd_vm_image_x86_64:
  tags:
    - x86_64
  <<: *vm_image
  variables:
    ARCH: x86_64
    TYPE: minimal-systemd

.flatpak_runtimes_publish_template: &flatpak_runtimes_publish
  script:
    - cd "${CI_PROJECT_DIR}"/sdk
    - ${BST} -o target_arch "${ARCH}" fetch all.bst
    - ${BST} -o target_arch "${ARCH}" build all.bst
    - flatpakarch="${ARCH/i586/i386}"
    - mkdir runtimes
    - |
      for runtime in sdk{,-debug,-docs,-locale} platform{,-locale,-arch-libs{,-debug},-vaapi} glxinfo{,-debug} basesdk{,-debug,-locale} baseplatform{,-locale}; do
        if [ "${runtime}" = "platform-vaapi" ]; then
           if [ "${ARCH}" = "aarch64" ] || [ "${ARCH}" = "Armv7" ]; then
             continue
           fi
        fi
        bst -o target_arch "${ARCH}" checkout --hardlinks "${runtime}.bst" "runtimes/${runtime}";
      done
    - echo "Clone the releases OSTree repo locally"
    - ostree init --repo=releases --mode=archive-z2
    - ostree remote add --repo=releases origin https://cache.sdk.freedesktop.org/releases/ --no-gpg-verify
    - ostree pull --repo=releases origin --mirror

    - echo "Commit the binaries to the correct branch"
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.BaseSdk/"${flatpakarch}"/unstable runtimes/basesdk
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.BaseSdk.Locale/"${flatpakarch}"/unstable runtimes/basesdk-locale
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.BaseSdk.Debug/"${flatpakarch}"/unstable runtimes/basesdk-debug
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.BasePlatform/"${flatpakarch}"/unstable runtimes/baseplatform
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.BasePlatform.Locale/"${flatpakarch}"/unstable runtimes/baseplatform-locale
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.Sdk/"${flatpakarch}"/unstable runtimes/sdk
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.Sdk.Debug/"${flatpakarch}"/unstable runtimes/sdk-debug
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.Sdk.Docs/"${flatpakarch}"/unstable runtimes/sdk-docs
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.Sdk.Locale/"${flatpakarch}"/unstable runtimes/sdk-locale
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.Platform/"${flatpakarch}"/unstable runtimes/platform
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.Platform.Locale/"${flatpakarch}"/unstable runtimes/platform-locale
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.GlxInfo/"${flatpakarch}"/unstable runtimes/glxinfo
    - ostree commit --repo=releases --branch=runtime/org.freedesktop.GlxInfo.Debug/"${flatpakarch}"/unstable runtimes/glxinfo-debug
    - |
      case "${ARCH}" in
        "x86_64"|"i586")
          ostree commit --repo=releases --branch=runtime/org.freedesktop.Platform.VAAPI.Intel/"${flatpakarch}"/unstable runtimes/platform-vaapi
          ;;
      esac
    - |
      case "${ARCH}" in
        "i586")
          ostree commit --repo=releases --branch=runtime/org.freedesktop.Platform.Compat."${flatpakarch}"/x86_64/unstable runtimes/platform-arch-libs
          ostree commit --repo=releases --branch=runtime/org.freedesktop.Platform.Compat."${flatpakarch}".Debug/x86_64/unstable runtimes/platform-arch-libs-debug
          ;;
        "arm")
          ostree commit --repo=releases --branch=runtime/org.freedesktop.Platform.Compat."${flatpakarch}"/aarch64/unstable runtimes/platform-arch-libs
          ostree commit --repo=releases --branch=runtime/org.freedesktop.Platform.Compat."${flatpakarch}".Debug/aarch64/unstable runtimes/platform-arch-libs-debug
          ;;
      esac
    - echo "Push to the releases ostree repo"
    - export OSTREE_PUSH_SHA='9aa82b67325786a810653155b952a17b7ccc436a'
    - git clone https://github.com/ssssam/ostree-push.git
    - git -C ostree-push/ checkout "${OSTREE_PUSH_SHA}"
    - ostree-push/ostree-push --repo=releases ssh://releases@"${BST_CACHE_SERVER_ADDRESS}"
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  only:
    - master
  <<: *gitlab_cache_pull_push

publish_x86_64:
  stage: publish_x86_64
  <<: *flatpak_runtimes_publish
  tags:
    - x86_64
  variables:
    ARCH: x86_64

publish_i586:
  stage: publish_i586
  <<: *flatpak_runtimes_publish
  tags:
    - x86_64
  variables:
    ARCH: i586

publish_aarch64:
  stage: publish_aarch64
  image: buildstream/buildstream-fedora:aarch64-master-81-06ae434
  <<: *flatpak_runtimes_publish
  tags:
    - aarch64
  variables:
    ARCH: aarch64

publish_arm:
  stage: publish_arm
  image: buildstream/buildstream-fedora:aarch64-master-81-06ae434
  <<: *flatpak_runtimes_publish
  tags:
    - armhf
  variables:
    ARCH: arm
